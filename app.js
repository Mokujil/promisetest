'use strict'

var axios = require('axios')
var cheerio = require('cheerio')
const tab = ['http://google.fr', 'http://yahoo.fr', 'http://bing.fr']

var result = []

var promises = tab.map(function(obj) {
	return fetch(obj).then(response => {
		return response.data
	})
	.then(response => {
		return parse(response).then(response => {
			var resp = []
			resp[obj] = response
			return resp
		})
	})
	.catch(reason => {
		console.log(reason)
	})
})

Promise.all(promises).then(function(results) {
    console.log(results)
})

console.log(result)

function fetch(obj) {
	return axios.get(obj)
}

function parse(html) {
	var $ = cheerio.load(html)
	var fields = []
	return new Promise(function(resolve, reject) {
		fields['title'] = $('title').html()
		fields['content'] = 'content'
		resolve(fields)
	})
}
